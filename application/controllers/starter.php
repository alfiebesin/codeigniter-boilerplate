<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Starter extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		
	}

	public function index()
	{
		
	}

	/**
	 * Func to send email
	 */
	function sendemail()
	{

		$email_config= Array(
			'protocol' => 'smtp',
			'smtp_host' => 'asia6.myserverhosts.com', // edit this
			'smtp_port' => 465,
			'smtp_crypto' => 'ssl',
			'smtp_user' => 'no-reply@bloqresidences.ph', // edit this
			'smtp_pass' => 'clicking123', // edit this
			'mailtype'  => 'html', 
			'charset'   => 'iso-8859-1' // edit this
		); 

		$this->load->library('email', $email_config);

		// send email
		$this->email->from('no-reply@bloqresidences.ph', 'noreply');
		$this->email->to('alfiebesin@gmail.com');
		$this->email->subject('A sunny hello from Bloq Residences!');
		$this->email->message('Touch enabled jQuery plugin that lets you create beautiful responsive ... A jquery plugin that will allow you ');

		$this->email->send();
	}

	/**
	 * Func to show upload form
	 */
	function upload()
	{
		$this->load->view('starter/upload/upload_form_view', array('error'=>''));
	}

	/**
	 * Func to do upload files to remote server
	 */
	function do_upload()
	{
		$config['upload_path'] = './user_uploads/'; // create this directory on your remote server
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']	= '1024';
		// $config['max_width']  = '1024';
		// $config['max_height']  = '768';

		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload())
		{
			$error = array('error' => $this->upload->display_errors());

			$this->load->view('starter/upload/upload_form_view', $error);
		}
		else
		{
			$data = array('upload_data' => $this->upload->data());

			$this->load->view('starter/upload/upload_form_success', $data);
		}
	}

}

/* End of file starter.php */
/* Location: ./application/controllers/starter.php */