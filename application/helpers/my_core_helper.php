<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

date_default_timezone_set('Asia/Hong_Kong');    //current timezone in the philippines

/*
* Fct to get the date with the correct format used in the db
*/
function get_date($date_to_format = ''){
    $date = empty($date_to_format) ? time() : $date_to_format;
    return date('Y-m-d H:i:s', $date);
}//endfct

